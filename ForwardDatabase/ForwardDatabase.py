import re
import json
ports = 0
fwd_db = {}
frame_discard ="Frame Discarded"
frame_sentonPort ="Frame sent on port "
frame_sentonAllPort ="No Update; Frame broadcast on all out ports"
frame_update ="FDB updated; Frame sent on port "
output = open("fwd_output.txt", mode = "w")

#Decide the route
def route_message (content, value, neighbour, new):
    dest_port_value = fwd_db.get(data[neighbour + 1])
    source_des_value = fwd_db.get(content)

    if new:
        if dest_port_value == source_des_value:
            write_output(content, neighbour, "FDB Updated; Discard Frame")
            return
    if new:
        if dest_port_value != None:
            write_output(content, neighbour, frame_update + str(dest_port_value))
            return
        else:
            write_output(content, neighbour, "FDB Updated; " + frame_sentonAllPort)
            return
    if dest_port_value != None:
        if dest_port_value == value:
            write_output(content, neighbour, frame_discard)
            return
        else:
            write_output(content, neighbour, frame_sentonPort + str(dest_port_value))
            return
    else:
         write_output(content, neighbour, frame_sentonAllPort)
         return

#write the output file
def write_output (content, neighbour, message):
    output.write(content)
    output.write("\t")
    output.write(data[neighbour + 1])
    output.write("\t")
    output.write(data[neighbour + 2])
    output.write("\t")
    output.write(message)
    output.write("\n")

#Read the file
with open("fwd_input.txt", mode = "r") as file:
    data = file.read()
    data = re.sub('[^a-zA-Z0-9\.]', '', data)
    ports = data[0]
#store the content of file as a key value pair
for content in data[1::2]:
    neighbour = data.index(content)
    fwd_db[content] = data[neighbour + 1]


#read the routing info file
with open("routing_info.txt", mode = "r") as file:
    data = file.read()
    data = re.sub('[^a-zA-Z0-9\.]', '', data)
index = 0
for content in data[0::3]:
    value = fwd_db.get(content)
    if value != None:
        route_message(content, value, index, new = False)
    else:
        #update fwd_db
        fwd_db[content] = data[index + 2]
        route_message (content, value, index, new = True)
    index += 3

#write the updated database to file
output.write("\n")
output.write("The updated FDB Table is: ")
output.write("\n")
output.write("\n")
output.write(json.dumps(fwd_db))





